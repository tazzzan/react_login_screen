import React from 'react';
import {InputField} from "../../../core/InputField";

export const LoginPanelInputForm = ({value, setUserName, setPassword}) => {

    return (
        <div style={style}>
            <InputField
                value={value}
                setValue={setUserName}/>
            <InputField
                value={value}
                setValue={setPassword}/>
        </div>
    );

};

const primaryYellow = '#E2BB22';

const style = {
    backgroundColor: primaryYellow,
    height: '30vh',
    width: '70vw',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
};
