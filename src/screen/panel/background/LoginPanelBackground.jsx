import React, {useState} from 'react';
import {LoginPanelButton} from "../button/LoginPanelButton";

export const LoginPanelBackground = ({children, userName}) => {

    const [showInput, setShowInput] = useState(false);

    return (
        <div style={style}>
            <h2 style={titleStyle}>Login</h2>
            {children}
            {
                showInput && (
                    userName.length !== 0 ?
                        <div style={greetingStyle}>
                            Hello {userName}!
                        </div> :
                        <div style={greetingStyle}>
                            Please enter your user name!
                        </div>
                )
            }
            <div style={buttonRowStyle}>
                <LoginPanelButton onClick={onShowEnteredInput}/>
                <LoginPanelButton onClick={onShowEnteredInput}/>
            </div>
        </div>
    )

    function onShowEnteredInput() {
        setShowInput(true);
    }
};

const primaryDark = '#15202B';

const style = {
    backgroundColor: primaryDark,
    height: '80vh',
    width: '70vw',
    borderRadius: '35px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
};

const titleStyle = {
    marginTop: '10vh',
    color: '#FFFFFF',
};

const greetingStyle = {
    color: '#FFFFFF',
};

const buttonRowStyle = {
    marginBottom: '2vh',
    width: '50vw',
    display: 'flex',
    justifyContent: 'space-between'
};
