import React from 'react';
import {CircleButton} from "../../../core/CircleButton";

export const LoginPanelButton = ({onClick}) => {
    return (
        <div style={style}>
            <CircleButton
                withBorder='true'
                onClick={onClick}/>
        </div>
    );
}

const style = {
    display: 'flex',
    alignItems: 'center',
};
