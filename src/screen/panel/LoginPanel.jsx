import React from 'react';

export const LoginPanel = ({children}) => {
    return (
        <div style={style}>
            {children}
        </div>
    )
}

const style = {
    backgroundColor: '#E2BB22',
    height: '50vh',
    width: '85vw',
    borderRadius: '35px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
};

