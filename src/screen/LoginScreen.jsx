import React, {useState} from 'react';
import {LoginPanel} from "./panel/LoginPanel";
import {LoginPanelBackground} from "./panel/background/LoginPanelBackground";
import {LoginPanelInputForm} from "./panel/background/LoginPanelInputForm";


export const LoginScreen = () => {

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    return (
        <div style={style}>
            <LoginPanel>
                <LoginPanelBackground userName={userName}>
                    <LoginPanelInputForm
                        setUserName={setUserName}
                        setPassword={setPassword}/>
                </LoginPanelBackground>
            </LoginPanel>
        </div>
    );
}

const thirdGreen = '#AEA985';

const style = {
    height: '100vh',
    backgroundColor: thirdGreen,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
};
