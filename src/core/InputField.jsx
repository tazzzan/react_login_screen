import React from 'react';

export const InputField = ({value, setValue}) => {
    return (
        <input style={style}
               type="text"
               value={value}
               onChange={event => setValue(event.target.value)}
        />
    );
};

const style = {
    padding: '12px',
    fontSize: '16px',
    height: '3vh',
    width: '60vw',
    borderRadius: '25px',
    display: 'flex',
    adjustItems: 'center',
    marginTop: '8px',
    marginBottom: '8px',
    backgroundColor: '#15202B',
    color: '#FFFFFF'
};
