import React from 'react';

export const CircleButton = ({onClick, withBorder}) => {
    return (
        <div style={outerButtonStyle}
             onClick={onClick}>
            {
                withBorder && <div style={innerButtonStyle}/>
            }
        </div>
    );
}

const outerButtonStyle = {
    backgroundColor: '#AEA985',
    borderRadius: "50%",
    width: '14vw',
    height: '14vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
};

const innerButtonStyle = {
    backgroundColor: '#FFFFFF',
    borderRadius: "50%",
    width: '10vw',
    height: '10vw',
};
